# 毒鸡汤

静态页面版 [egotong/nows][nows]，托管在 Gitee Pages。

Gitee Pages 访问地址：<http://liyupeng00.gitee.io>

毒鸡汤 访问地址：<http://liyupeng00.gitee.io/dujitang/>

## 语录参考链接

- <https://www.zhihu.com/question/43340253>

## Credits

* [egotong/nows][nows]
* [jitang.fun](http://jitang.fun/)


---

欢迎接收更多好玩的东西。

<div align="center"><img width="192px" height="192px" src="/assets/TheStarryNigh_qrcode.png"/></div>


[nows]: https://github.com/egotong/nows


